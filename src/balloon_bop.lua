local S = minetest.get_translator("aes_achievements")

if not minetest.get_modpath("balloon_bop") then return end

achvmt_lib.register_mod("balloon_bop", {
  name = arena_lib.mods["balloon_bop"].name,
  icon = "balloon_bop_hudballoon.png"
})


local achievements = {
  ["balloon_bop:pop"]   = { title = S("Pop!"), img = "bbop_pop.png", desc = S("Score @1 or more", 500), tier = "Bronze" },
  ["balloon_bop:pop_pop"] = { title = S("POP POP!"), img = "bbop_poppop.png", desc = S("Score @1 or more", 1000), tier = "Gold" },
}



for name, ach in pairs(achievements) do
  achvmt_lib.register_achievement(name, {
    title = ach.title,
    description = ach.desc,
    image = ach.img,
    tier = ach.tier,
    hidden = false,
  })
end



arena_lib.register_on_celebration(function(mod, arena, winner)
  if mod == "balloon_bop" and winner then
    local score = arena.players[winner].score

    if score >= 500 then
      achvmt_lib.award(winner, "balloon_bop:pop")
    end

    if score >= 1000 then
      achvmt_lib.award(winner, "balloon_bop:pop_pop")
    end
  end
end)